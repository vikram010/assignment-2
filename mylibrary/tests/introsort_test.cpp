#include <gtest/gtest.h>

#include "../introsort.h"

#include <vector>
#include <algorithm>



// Helper function
template <typename T>
::testing::AssertionResult VectorsMatch( const std::vector<T>& expected, const std::vector<T>& actual ){

  // Test size
  if( expected.size() != actual.size() )
    return ::testing::AssertionFailure() << "size mismatch: |vector| (" << actual.size() << ") != |expected| (" << expected.size() << ")";

  // Test per element content
  for( typename std::vector<T>::size_type i {0}; i < expected.size(); ++i )
    if( expected.at(i) != actual.at(i) )
      return ::testing::AssertionFailure() << "vector[" << i
                                           << "] (" << actual.at(i) << ") != expected[" << i
                                           << "] (" << expected.at(i) << ")";

  return ::testing::AssertionSuccess();
}


// Vector conatiner initialization tests
TEST(Algorithm_introsort,SortTest_01) {

  // Define gold
  std::initializer_list<int> data {4,1,3,2};
  std::initializer_list<int> gold {1,2,3,4};



  // Construct
  std::vector<int> vecgold {gold};
  std::vector<int> vecdata {data};


  // TEST: Sort the vector using < comparison (default)
//  mylib::introsort(vec.begin(),vec.end());
  EXPECT_TRUE (VectorsMatch(vecgold,vecdata));

}


