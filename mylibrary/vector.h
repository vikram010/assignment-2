#ifndef __VECTOR_H__
#define __VECTOR_H__



#include <initializer_list>
#include <memory>
#include <stdexcept>
#include <string>

namespace mylib {




template <typename Elem, typename Alloc = std::allocator<Elem>>  // requires Element<Elem>() && Allocator<Aloc>()
struct Vector_Base {
  using size_type       = size_t;

  Alloc                 _alloc;     // memory allocator
  Elem*                 _elements;  // pointer to first element
  Elem*                 _next;      // pointer to first unused (and unitialized) slot
  Elem*                 _last;      // pointer to last slot

  Vector_Base();
  Vector_Base( const Alloc& alloc, size_type n );
  Vector_Base( std::initializer_list<Elem> lst );
  Vector_Base( Vector_Base&& rvalue );
  ~Vector_Base();

  Vector_Base& operator=( Vector_Base&& rvalue ) = default;
  

}; // END class Vector_Base




template <typename Elem, typename Alloc>
Vector_Base<Elem,Alloc>::Vector_Base() 
  : _alloc{}, _elements {nullptr}, _next {nullptr}, _last {nullptr} 
{}

template <typename Elem, typename Alloc>
Vector_Base<Elem,Alloc>::Vector_Base( const Alloc& alloc, size_type n ) 
  : _alloc{alloc}, _elements {_alloc.allocate(n)}, _next {_elements}, _last {_elements+n} 
{}


template <typename Elem, typename Alloc>
Vector_Base<Elem,Alloc>::Vector_Base( std::initializer_list<Elem> lst )  
  : _alloc {},_elements {_alloc.allocate(lst.size())}, _next {nullptr}, _last {nullptr} 
{
  
  std::copy(lst.begin(),lst.end(),_elements);
  _last = _next = _elements + lst.size();
}

template <typename Elem, typename Alloc>
Vector_Base<Elem,Alloc>::Vector_Base( Vector_Base<Elem,Alloc>&& rvalue ) 
  : _alloc{std::move(rvalue._alloc)},
    _elements{rvalue._elements},
    _next{rvalue._next},
    _last{rvalue._last} 
{

  rvalue._last = rvalue._next = rvalue._elements = nullptr;
}

template <typename Elem, typename Alloc>
Vector_Base<Elem,Alloc>::~Vector_Base() {

  _alloc.deallocate(_elements,_last-_elements);
}






template <typename Elem, typename Alloc = std::allocator<Elem>>  // requires Element<Elem>() && Allocator<Aloc>()
class Vector : private Vector_Base<Elem,Alloc> {
public:

  // Types
  using iterator        = Elem*;
  using const_iterator  = const Elem*;
  using size_type       = typename Vector_Base<Elem,Alloc>::size_type;

  // Constructors
  using Vector_Base<Elem,Alloc>::Vector_Base;

  // Data
  void                  reserve( size_type n );
  void                  resize( size_type n, const Elem& e = Elem() );
  void                  push_back( const Elem& e );

  // Info
  size_type             capacity() const;
  size_type             size() const;
 
  // Member access
  const Elem&           at( size_type n ) const;
  Elem&                 operator [] ( size_type n );

  // Iterators
  iterator              begin();
  const_iterator        begin() const;
  iterator              end();
  const_iterator        end() const;

}; // END class Vector






template <typename Elem, typename Alloc>
void 
Vector<Elem,Alloc>::push_back( const Elem& e ) {

  // Increase capacity
  if     ( capacity() == 0 )      reserve(8);
  else if( capacity() == size() ) reserve(2*capacity());
  
  // Push element on end and update space pointer
  this->_alloc.construct(&this->_elements[size()],e);
  this->_next = this->_elements + size() + 1;
}

template <typename Elem, typename Alloc>
void 
Vector<Elem,Alloc>::reserve( size_type n ) {

  if( n <= capacity() ) return;

  // Allocate new memory
  Vector_Base<Elem,Alloc> b(this->_alloc,n);

  // Copy over and delete old data
  std::uninitialized_copy(b._elements,&b._elements[size()],this->_elements);
  for(size_type i {0}; i < size(); ++i) this->_alloc.destroy(&this->_elements[i]);

  // Swap representation
  std::swap<Vector_Base<Elem,Alloc>>(*this,b);
}

template <typename Elem, typename Alloc>
void 
Vector<Elem,Alloc>::resize( size_type n, const Elem& e ) {
  
  reserve(n);
  for( size_type i {size()}; i < n; ++i ) this->_alloc.construct(&this->_elements[i],e);
  for( size_type i {n}; i < size(); ++i ) this->_alloc.destruct(&this->_elements[i]);
}

template <typename Elem, typename Alloc>
typename Vector<Elem,Alloc>::size_type
Vector<Elem,Alloc>::capacity() const { return this->_last - this->_elements; }

template <typename Elem, typename Alloc>
typename Vector<Elem,Alloc>::size_type
Vector<Elem,Alloc>::size() const { return this->_next - this->_elements; }

template <typename Elem, typename Alloc>
const Elem&
Vector<Elem,Alloc>::at( size_type n ) const { 

  if( n < 0 or n >= size() ) throw std::out_of_range( "mylib::Vector out of range; size() = " 
                                                      + std::to_string(size()) + ", n = " + std::to_string(n) ); 
  return *(this->_elements+n); 
}

template <typename Elem, typename Alloc>
Elem&
Vector<Elem,Alloc>::operator[] ( size_type n ) { return *(this->_elements+n); }

template <typename Elem, typename Alloc>
typename Vector<Elem,Alloc>::iterator
Vector<Elem,Alloc>::begin() { return this->_elements; }

template <typename Elem, typename Alloc>
typename Vector<Elem,Alloc>::const_iterator
Vector<Elem,Alloc>::begin() const { return this->_elements; }

template <typename Elem, typename Alloc>
typename Vector<Elem,Alloc>::iterator
Vector<Elem,Alloc>::end() { return this->_next; }

template <typename Elem, typename Alloc>
typename Vector<Elem,Alloc>::const_iterator
Vector<Elem,Alloc>::end() const { return this->_next; }


} // END namespace mylib

#endif // __VECTOR_H__
