# Set a project name an version number for our library
project(mylibrary VERSION 1.0)

# List header files
set(HDRS
  benchmark.h
  functions.h
  bubblesort.h
  shellsort.h
  vector.h
  list.h
  selectionsort.h
 introsort.h
)

# List source files
set(SRCS
  functions.cpp
  list.cpp
)



# Add rules to create a library
add_library(${PROJECT_NAME} SHARED ${SRCS} ${HDRS})
#add_library(${PROJECT_NAME} STATIC ${SRCS} ${HDRS})

# Tell the compiler to use the our compile flags
set_target_properties(${PROJECT_NAME} PROPERTIES COMPILE_FLAGS ${MY_COMPILE_FLAGS})




# Unit testing
if(GTEST_FOUND)

  # Add test
  ADD_TESTS(shellsort_tests ${PROJECT_NAME})
  ADD_TESTS(selectionsort_tests ${PROJECT_NAME})
  ADD_TESTS(vector_tests ${PROJECT_NAME})
  ADD_TESTS(list_tests ${PROJECT_NAME})
  ADD_TESTS(introsort_test ${PROJECT_NAME})



endif(GTEST_FOUND)
