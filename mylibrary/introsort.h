#ifndef INTROSORT
#define INTROSORT
#include<vector>
#include<iterator>
#include<cmath>
#include<functional>
#include<algorithm>
#include"bubblesort.h"
#include"selectionsort.h"
#include"shellsort.h"
#include"introsort.h"

namespace mylib
{
    template <class RandomAcessIterator, class Compare = std::less<typename RandomAcessIterator::value_type>>
    void looping_introsort( RandomAcessIterator first, RandomAcessIterator last, double limit, Compare cmp = Compare())
    {
        int size = 5;
        while (last-first>size)
        {
            if(limit == 0)
            {
            selection_sort(first,last);
            return;
            }
            else
            {
            --limit;
            RandomAcessIterator temp = (first + (last-first)/2);
            looping_introsort( temp, last, limit);
            last = temp;
            }
        }
    }

    template <class RandomAcessIterator, class Compare = std::less<typename RandomAcessIterator::value_type>>
    void introsort(RandomAcessIterator first, RandomAcessIterator last, Compare cmp = Compare())
    {
        using size_type = typename RandomAcessIterator::difference_type;
        size_type depth = 2 * std::floor(std::log2(last-first));
        looping_introsort(first,last,depth,cmp);
        bubbleSort(first,last);
    }
}

#endif // INTROSORT

