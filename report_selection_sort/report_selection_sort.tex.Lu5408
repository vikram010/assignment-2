% What kind of text document should we build
\documentclass[a4,10pt]{article}


% Include packages we need for different features (the less, the better)

% Clever cross-referencing
\usepackage{cleveref}


\providecommand{lstlisting}
% Math
\usepackage{amsmath}

% Algorithms
\usepackage{algorithm}
\usepackage{algpseudocode}

% Tikz
\RequirePackage{tikz}
\usetikzlibrary{arrows,shapes,calc,through,intersections,decorations.markings,positioning}

\tikzstyle{every picture}+=[remember picture]

\RequirePackage{pgfplots}









% Set TITLE, AUTHOR and DATE
\title{Report of Introsort Algorothm}
\author{Bikram Bam}
\date{\today}



\begin{document}



  % Create the main title section
  \maketitle

  \begin{abstract}
   This repots contains the implementation of  different sorting  algorithm and comparing with
   each other on the basis of performance and time. The  performance of algorithm
   are presented on the graph and compared.
  \end{abstract}


  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %%  The main content of the report  %%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  \section{Introduction}
  Introsort is a hybrid sorting algorithm that provides both fast average performance and
  optimal worst-case performance. It begins with quicksort and switches to heapsort when
  the recursion depth exceeds a level based on the number of elements being sorted.
  This combines the good parts of both algorithms, with practical performance comparable
  to quicksort on typical data sets and worst-case O(n log n) runtime due to the heap sort.
  Intro sort is one of the fastest way to sort the values. Its an alternative of Quick Sort.
  Intro sort play a best part in worst case of quick sort algorithm but at average case its
  time bound, which is 2 -5 times longer than quick sort. Quick Sort partition sequence of
  size N to half and and divide those next sequence to i.e N/4 and so on but intro sort
  avoid the problem by putting bound on depth and switch to Shell Sort.
  \cite{shell:1959}

\section{program}
\begin {lstlisting}
The program for introsort loop is below:
void introsort_loop( RandomAcessIterator first,
RandomAcessIterator last, double limit, Compare cmp = Compare())
{
    int size_threshold = 3;
    while (last-first>size_threshold)
    {
        if(limit == 0)
        {
            selection_sort(first,last);
            return;
        }
        else
        {
            --limit;
            RandomAcessIterator cut = (first + (last-first)/2);
            introsort_loop( cut, last, limit);
            last = cut;
        }
    }
}

template <class RandomAcessIterator,
class Compare = std::less<typename RandomAcessIterator::value_type>>



void introsort(RandomAcessIterator first, RandomAcessIterator last, Compare cmp = Compare())
{
    using size_type = typename RandomAcessIterator::difference_type;
    size_type depth_limit = 2 * std::floor(std::log2(last-first));

    introsort_loop(first,last,depth_limit,cmp);
    bubbleSort(first,last);
}

}



\end{lstlisting}





  \section{Benchmark set-up}
  Describe how the benchmark was performed. Be objective - don't describe too many details.
  The reader which this text is meant for should be able to reconstruct the experiment using his own tools.
  Some examples of topics for this section:
    \begin{itemize}
      \item All tests were performed on a computer with the following specifications...
      \item For the container we have measured how much time the CPU has spent on the \texttt{push\_back()} function. The emplace operation (construct and insert element) was used to allocate the storage.
      \item For the sorting algorithms, a data set of $N$ integers were sorted, where duration times were measured by using the high precision chrono device which is included with the STL.
    \end{itemize}

  \section{Results}

    \begin{figure}%[width=\textwidth]
      \begin{tikzpicture}
        \begin{axis}[
          xmajorgrids=false,ymajorgrids=false,
          legend pos=north west,
          width=0.9\textwidth, height=0.25\textheight,
%          reverse legend
          ]

          \addplot[green] table[x=size ,y=time, skip first n=0] {dat/benchmark_stl_sort.dat};
          \addplot[red] table[x=size ,y=time, skip first n=0] {dat/benchmark_mylib_bubble.dat};
          \addplot[blue] table[x=size ,y=time, skip first n=0] {dat/benchmark_mylib_shell.dat};
          \addplot[pink] table[x=size ,y=time, skip first n=0] {dat/benchmark_mylib_selection_sort.dat};
          \addplot[yellow] table[x=size ,y=time, skip first n=0] {dat/benchmark_mylib_intro_sort.dat};
          \legend{
            STL sort,
            bubble,
            Shell,
            Selection,
            Intro
          }
        \end{axis}
      \end{tikzpicture}
      \caption{My accurate and descriptive caption!}
      \label{fig:bench_sort}
    \end{figure}

  \section{Concluding remarks}
  \begin{itemize}
    \item Reflect over the method and results.
    \item Topics for future work could be suggested here.
  \end{itemize}
  Example: By comparing the performance of the reserve and emplace functionality of the STL version of the vector to our custom vector implementation from \emph{mylib} we conclude that the STL vector is slightly faster at the specified operation.



  % Include the bibliography
  \bibliographystyle{plain}
  \bibliography{bibliography}

\end{document}
